FROM eclipse-temurin:17 AS build
COPY ./ /app/
WORKDIR /app/
RUN ./mvnw package

FROM eclipse-temurin:17-jre AS prod
COPY --from=build /app/target/*.jar /app/hello.jar
CMD java -jar /app/hello.jar
